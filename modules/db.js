const mongoose = require('mongoose');
mongoose.connect('mongodb://kite:1234@127.0.0.1:27017/movies',{ useUnifiedTopology: true, useNewUrlParser: true })
.then(() => {console.log('数据库连接成功');})
.catch((err) => {console.log('数据库连接失败:'+err);})

module.exports = mongoose;