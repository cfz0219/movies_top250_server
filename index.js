const Koa = require('koa');
const app = new Koa();
const cors = require('koa2-cors');
const bodyParser = require('koa-bodyparser');
const static = require('koa-static');

app.use(static(`${process.cwd()}/static`));

//配置图片上传的中间件
const koaBody = require('koa-body');

app.use(koaBody({
  // 支持文件格式
  multipart: true,
  formidable: {
      maxFileSize: 200*1024*1024,
      // 保留文件扩展名
  keepExtensions: true
  }
}));


// 处理post请求中间件
app.use(bodyParser());
// 处理跨域中间件
app.use(cors());

//  路由
app.use(require('./routes/index').routes())

app.listen(8080, () =>  {
  console.log('服务器启动成功: http://127.0.0.1:8080');
});