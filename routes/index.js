const router = require('koa-router')();
const fs = require('fs');
const path = require('path');
const Top250Model = require('../modules/top250');

// 获取数据接口
router.get('/top250', async ctx => {
  let {start, limit} = ctx.query;
  // 起始页
  if(start == undefined) {
    start = 0;
  }
  // 每页条数
  if(limit == undefined) {
    limit = 5;
  }
  const res = await Top250Model.find().skip(Number(start)).limit(Number(limit));
  const total = await Top250Model.find().countDocuments();
  ctx.body = {
    code: 200,
    data: res,
    total,
    msg: 'Get/ Top250, 请求成功'
  }
})

// 收藏接口
router.post('/collect', async ctx => {
  const id = ctx.request.body.id;
  const res = await Top250Model.updateOne({_id: id}, { collected:true });
  if(res.nModified == 1) {
    ctx.body = {
      code: 200,
      msg: '收藏成功'
    }
  }else if(res.nModified == 0) {
    ctx.body = {
      code: 403,
      msg: '重复收藏'
    } 
  }
})
// 取消收藏接口
router.post('/collect/cancel', async ctx => {
  const id = ctx.request.body.id;
  const res = await Top250Model.updateOne({_id: id}, { collected:false });
  if(res.nModified == 1) {
    ctx.body = {
      code: 200,
      msg: '取消收藏'
    }
  }else if(res.nModified == 0) {
    ctx.body = {
      code: 403,
      msg: '重复取消'
    } 
  }
})

// 删除接口
router.post('/delete', async ctx => {
  // console.log(ctx.request.body);
  const id = ctx.request.body.id;
  const res = await Top250Model.deleteOne({_id:id});
  console.log(res);
  if(res.deletedCount == 1) {
    ctx.body = {
      code: 200,
      msg: '删除成功'
    }
  }
})

// 图片上传接口
router.post('/doUpload', async ctx => {
  //1.将本地图片存到服务器上
  const file = ctx.request.files.file
  const basename = path.basename(file.path)
  // 创建可读流
  const reader = fs.createReadStream(file.path);
  // 获取上传文件扩展名
  let filePath = process.cwd() + `/static/${basename}`;
  // 创建可写流
  const upStream = fs.createWriteStream(filePath);
  // 可读流通过管道写入可写流
  reader.pipe(upStream);
  let pic = `${ctx.origin}/${basename}`
  ctx.body = {
    code: 200,
    url: pic,
    msg:'上传成功'
  }
})
// 表单提交接口
router.post('/doAdd', async ctx => {
   /* 那么这里可以取得文字相关的信息 */
   const {title,pic,slogo,evaluate,rating,labels,collected} = ctx.request.body;
   let data = new Top250Model({
        title,
        pic,
        slogo,
        evaluate,
        rating,
        labels,
        collected:Boolean(collected)
    })
    const res = await data.save()
    if(res){
      ctx.body = {
        code: 200,
        msg:'提交成功'
      }
    }
    
  
})

// 根据id查询电影
router.get('/detail', async ctx => {
  const id = ctx.request.query.id.trim();
  const res = await Top250Model.find({_id: id})
  ctx.body = {
    code:200,
    data: res,
    msg: 'GET /detail 电影详情'
  }
})

module.exports = router